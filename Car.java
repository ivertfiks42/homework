public class Car {
    private void startElectricity(){
        System.out.println("Электроника запущена");
    }

    private void startCommand(){
        System.out.println("Двигатель запущен");
    }

    private void startFuelSystem(){
        System.out.println("Подача топлива запущена");
    }

    public void start(){
        startElectricity();
        startCommand();
        startFuelSystem();
    }
}
